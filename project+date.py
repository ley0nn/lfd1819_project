import pandas as pd
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC, SVC
from sklearn.metrics import accuracy_score, f1_score, classification_report
from sklearn.utils import shuffle
import re
from datetime import datetime
from nltk.corpus import stopwords
import numpy as np
from numpy import argmax
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder

def date_to_feature(train):
    '''This tries to convert the date to a onehot vector to use in the feature union'''
    ##########DOESNT WORK YET THOUGH FUCKKKKK###############
    date = train['date']
    #print(date)
    label_encoder = LabelEncoder()
    integer_encoded = label_encoder.fit_transform(date)
    #print(integer_encoded)
    onehot_encoder = OneHotEncoder(sparse=False)
    integer_encoded = integer_encoded.reshape(len(integer_encoded), 1)
    onehot_encoded = onehot_encoder.fit_transform(integer_encoded)
    
    return onehot_encoded



def remove_stopWords(s):
    '''For removing stop words'''
    stop_words = set(stopwords.words('english'))
    s = ' '.join(word for word in s.split() if word not in stop_words)
    return s

def preprocess(df):

    df.loc[:, "raw_text"] = df.text.apply(lambda x: str.lower(x)) # lowering
    df.loc[:, "raw_text"] = df.text.apply(lambda x: str.strip(x)) # strip
    #df.loc[:, "raw_text"] = df.text.apply(lambda x: " ".join(re.findall('[\w]+', x))) # removing punctuation
    df.loc[:, "raw_text"] = df.text.apply(lambda x: remove_stopWords(x)) # removing stopwords

    #print(df.loc[:, "date"])


    return df

def train_test(xtrain,ytrain,xtest,ytest):
    #added character N-grams as a feature and changed classifier to a linear svc (not with a reason, just performs better imo)
    pipeline = Pipeline([('features', FeatureUnion([
                                                ('token n-grams', TfidfVectorizer(binary=True, analyzer='word', max_df=0.8)),
                                                ('char n-grams', TfidfVectorizer(analyzer='char', ngram_range=(1,3)))
                                               ])), ('cls', LinearSVC())])
    

    pipeline.fit(xtrain, ytrain)
    yguess = pipeline.predict(xtest)
    print(classification_report(ytest,yguess))
    acc = accuracy_score(ytest, yguess)
    f1 = f1_score(ytest, yguess, average="macro")

    return acc, f1


if __name__ == '__main__':
    np.random.seed(1)
    train = pd.read_csv('data/hyperp-training-grouped.csv.xz',
                        compression='xz',
                        sep='\t',
                        encoding='utf-8',
                        index_col=0).dropna()


    
    train = preprocess(train)

    train = shuffle(train)
    onehot = date_to_feature(train)
    X = train['raw_text']
    X = np.column_stack((X,onehot))
    # Y = train['hyperp']
    Y = train['bias']

    split_point = int(0.75 * len(X))
    xtrain = X[:split_point]
    ytrain = Y[:split_point]
    xtest = X[split_point:]
    ytest = Y[split_point:]

    acc, f1 = train_test(xtrain, ytrain, xtest, ytest)

    print('\n## accuracy and f1_score:')
    print('accuracy: {:.3}'.format(acc))
    print('f1_score: {:.3}'.format(f1))