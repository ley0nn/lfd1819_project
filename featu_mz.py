# Learning from Data Project
# sup.: M. Nissim
#
# Leon Graumans, Wessel Reijngoud, Mike Zhang
# 23-10-2018
# revisions: 1

# command:
# python3 project.py [trainset] [testset] [-b] [-p] [-j]

from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import accuracy_score, f1_score, classification_report
from sklearn.model_selection import train_test_split
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.svm import LinearSVC

import sys
import argparse
import pandas as pd


class ItemSelector(BaseEstimator, TransformerMixin):
    '''https://stats.stackexchange.com/questions/177082/sklearn-combine-multiple-feature-sets-in-pipeline'''

    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, data_dict):
        return data_dict[self.key]

def read_data(path):
    '''function that reads the file in the path provided, also preprocesses it.'''

    df = pd.read_csv(path,
                           # compression='xz',
                           sep='\t',
                           encoding='utf-8',
                           index_col=0).dropna()

    df['raw_text'] = df.raw_text.apply(lambda x: str.lower(x))
    df['raw_text'] = df.raw_text.apply(lambda x: str.strip(x))

    return df

def print_results(ytest, yguess):
    '''function that prints the results of the classification.'''

    print(classification_report(ytest, yguess))
    print('\n## accuracy and f1_score:')
    print('accuracy: {:.3}'.format(accuracy_score(ytest, yguess)))
    print('f1_score: {:.3}'.format(f1_score(ytest, yguess, average='macro')))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Hyperpartisan classification task arguments')
    parser.add_argument('trainset', metavar='[trainset in .csv format]', type=str,
                        help='File containing training data')
    parser.add_argument('testset', nargs='?', default='', help='File containing test data')
    parser.add_argument('-b', '--bias', action='store_true',
                        help='Use bias classes')
    parser.add_argument('-p', '--hyperp', action='store_true',
                        help='Use hyperpartisan classes')
    parser.add_argument('-j', '--joint', action='store_true',
                        help='Use joint classes')
    args = parser.parse_args()

    if args.testset:
        print('### found testset, using it... ###')

        df_train = read_data(args.trainset)
        df_test = read_data(args.testset)

        xtrain = df_train.ix[:, ['raw_text']]
        ytrain = df_train.ix[:, ['bias', 'hyperp']]
        xtest = df_test.ix[:, ['raw_text']]
        ytest = df_test.ix[:, ['bias', 'hyperp']]

    else:
        print('### use trainset for training and testing... ###')

        df = read_data(args.trainset)

        df = df.sample(frac=0.1)  # shuffle & frac 0.2 voor 20% van je data

        X = df.ix[:, ['raw_text']]
        Y = df.ix[:, ['bias', 'hyperp']]

        xtrain, xtest, ytrain, ytest = train_test_split(X, Y, test_size=0.20, random_state=42)

    pipeline = Pipeline([
        ('union', FeatureUnion(
            transformer_list=[
                ('raw_text', Pipeline([
                    ('selector', ItemSelector(key='raw_text')),
                    ('tfidf', TfidfVectorizer(binary=True, ngram_range=(1,2))),
                ])),
        ])),
           ('svc', LinearSVC())
        ])

    if args.bias:
        print('Using bias classes..')
        pipeline.fit(xtrain, ytrain.bias)
        yguess_bias = pipeline.predict(xtest)
        print_results(ytest.bias, yguess_bias)

    elif args.hyperp:
        print('Using hyperpartisan classes..')
        pipeline.fit(xtrain, ytrain.hyperp)
        yguess_hyperp = pipeline.predict(xtest)
        print_results(ytest.hyperp, yguess_hyperp)

    elif args.joint:
        print('Using joint classes..')

        pipeline.fit(xtrain, ytrain.bias)
        yguess_bias = pipeline.predict(xtest)

        pipeline.fit(xtrain, ytrain.hyperp)
        yguess_hyperp = pipeline.predict(xtest)
        
        ytest_list = [str(val1) + ' ' + str(val2) for val1, val2 in zip(ytest.hyperp, ytest.bias)]
        yguess_list = [str(val1) + ' ' + str(val2) for val1, val2 in zip(yguess_hyperp, yguess_bias)]

        joint_labels = ['True left', 'False left-center', 'False least',  'False right-center', 'True right']

        print(classification_report(ytest_list, yguess_list, labels=joint_labels))
        print('\n## accuracy and f1_score:')
        print('accuracy: {:.3}'.format(accuracy_score(ytest_list, yguess_list)))
        print('f1_score: {:.3}'.format(f1_score(ytest_list, yguess_list, labels=joint_labels, average='macro')))

        outfile = open(sys.argv[0]+'_output.txt','w')
        print('Writing output to file..')
        for index, val1 in zip(xtest.raw_text.iteritems(), yguess_list):
            line = str(index[0]) + ' ' + val1.lower() + '\n'
            outfile.write(line)
        outfile.close()
        print('Done!')

        outfile.close()

    else:
        parser.print_help()
        



