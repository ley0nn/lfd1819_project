from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics import accuracy_score, f1_score, classification_report
from sklearn.model_selection import train_test_split
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.svm import LinearSVC
from sklearn.utils import resample

# from nltk.sentiment.vader import SentimentIntensityAnalyzer

import sys
import argparse
import pandas as pd


class ItemSelector(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, data_dict):
        return data_dict[self.key]


def preprocess(column):
    column = column.apply(lambda x: str.lower(x))
    column = column.apply(lambda x: str.strip(x))

    return column


def print_results(yguess, ytest):
    print(classification_report(ytest, yguess))
    print('\n## accuracy and f1_score:')
    print('accuracy: {:.3}'.format(accuracy_score(ytest, yguess)))
    print('f1_score: {:.3}'.format(f1_score(ytest, yguess, average='macro')))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Hyperpartisan classification task arguments')
    parser.add_argument('trainset', metavar='trainset', type=str,
                        help='File containing training data')
    parser.add_argument('testset', nargs='?', default='', help='File containing test data')
    parser.add_argument('-b', '--bias', action='store_true',
                        help='Use bias classes')
    parser.add_argument('-p', '--hyperp', action='store_true',
                        help='Use hyperpartisan classes')
    parser.add_argument('-j', '--joint', action='store_true',
                        help='Use joint classes')
    args = parser.parse_args()


    train = pd.read_csv(args.trainset,
                        # compression='xz',
                        sep='\t',
                        encoding='utf-8',
                        index_col=0).dropna()


    if args.testset:
        print('Found testset..')
        test = pd.read_csv(args.testset,
                        # compression='xz',
                        sep='\t',
                        encoding='utf-8',
                        index_col=0).dropna()

        train['raw_text'] = preprocess(train['raw_text'])
        test['raw_text'] = preprocess(test['raw_text'])

        xtrain = train.ix[:, ['raw_text', 'date', 'text', 'sentiment']]
        ytrain = train.ix[:, ['bias', 'hyperp']]

        xtest = test.ix[:, ['raw_text', 'date', 'text', 'sentiment']]
        ytest = test.ix[:, ['bias', 'hyperp']]
        # ytest.loc[:,'bias'] = 'XXX'
        # ytest.loc[:,'hyperp'] = 'XXX'

    else:
        train = train.sample(frac=1, random_state=1) # shuffle & frac 0.2 voor 20% van je data

        # sent_score = SentimentIntensityAnalyzer()
        # train['sentiment'] = train.apply(lambda row: str(sent_score.polarity_scores(row.text)['compound']), axis=1)
        # ''' vergeet niet dat dit ook bij testset moet gebeuren! '''

        # X = train.ix[:, ['raw_text', 'date', 'text', 'sentiment']]
        X = train['raw_text']
        Y = train.ix[:, ['bias', 'hyperp']]

        

        xtrain, xtest, ytrain, ytest = train_test_split(X, Y, test_size=0.20)#, random_state=42)


    pipeline = Pipeline([
        ('union', FeatureUnion(
            transformer_list=[
                # ('text', Pipeline([
                #     ('selector', ItemSelector(key='text')),
                #     ('count_char', CountVectorizer(analyzer='char', ngram_range=(1,2))),
                # ])),
                ('raw_text', Pipeline([
                    # ('selector', ItemSelector(key='raw_text')),
                    ('tfidf', TfidfVectorizer(binary=True, ngram_range=(1,2))),
                ])),
                # ('date', Pipeline([
                #     ('selector', ItemSelector(key='date')),
                #     ('count', CountVectorizer(binary=True)),
                # ])),
                # ('sentiment', Pipeline([
                #     ('selector', ItemSelector(key='sentiment')),
                #     ('count', CountVectorizer()),
                # ])),
        ])),
           ('svc', LinearSVC())
        ])

    if args.bias:
        print('Using bias classes..')
        X = preprocess(X)
        pipeline.fit(xtrain, ytrain.bias)
        yguess_bias = pipeline.predict(xtest)
        print_results(yguess_bias, ytest.bias)

    elif args.hyperp:
        print('Using hyperpartisan classes..')
        pipeline.fit(xtrain, ytrain.hyperp)
        yguess_hyperp = pipeline.predict(xtest)
        print_results(yguess_hyperp, ytest.hyperp)

    elif args.joint:
        print('Using joint classes..')

        pipeline.fit(xtrain, ytrain.bias)
        yguess_bias = pipeline.predict(xtest)

        pipeline.fit(xtrain, ytrain.hyperp)
        yguess_hyperp = pipeline.predict(xtest)
        
        ytest_list = [str(val1) + ' ' + str(val2) for val1, val2 in zip(ytest.hyperp, ytest.bias)]
        yguess_list = [str(val1) + ' ' + str(val2) for val1, val2 in zip(yguess_hyperp, yguess_bias)]

        joint_labels = ['True left', 'False left-center', 'False least',  'False right-center', 'True right']
        print(classification_report(ytest_list, yguess_list, labels=joint_labels)) 
        print('\n## accuracy and f1_score:')
        print('accuracy: {:.3}'.format(accuracy_score(ytest_list, yguess_list)))
        print('f1_score: {:.3}'.format(f1_score(ytest_list, yguess_list, labels=joint_labels, average='macro')))

        # outfile = open(sys.argv[0]+'_output.txt','w')
        # print('Writing output to ', outfile.name, '..')
        # for index, val1 in zip(xtest.raw_text.iteritems(), yguess_list):
        #     line = str(index[0]) + ' ' + val1.lower() + '\n'
        #     outfile.write(line)
        # outfile.close()

    else:
        parser.print_help()
        



