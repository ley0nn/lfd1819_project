# Learning from Data Project
# hyperpartisan.py
# sup.: M. Nissim
#
# Leon Graumans, Wessel Reijngoud, Mike Zhang
# 23-10-2018
# revisions: 1

# command:
# python3 hyperpartisan.py trainset [testset] [-b] [-p] [-j]

from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import accuracy_score, f1_score, classification_report
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC

import sys
import argparse
import pandas as pd


def read_data(path):
    '''function that reads the file in the path provided, also preprocesses it.'''

    df = pd.read_csv(path,
                           # compression='xz',
                           sep='\t',
                           encoding='utf-8',
                           index_col=0).dropna()

    df['raw_text'] = df.raw_text.apply(lambda x: str.lower(x))
    df['raw_text'] = df.raw_text.apply(lambda x: str.strip(x))

    return df

def print_results(ytest, yguess):
    '''function that prints the results of the classification.'''

    print(classification_report(ytest, yguess))
    print('\n## accuracy and f1_score:')
    print('accuracy: {:.3}'.format(accuracy_score(ytest, yguess)))
    print('f1_score: {:.3}'.format(f1_score(ytest, yguess, average='macro')))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Hyperpartisan classification task arguments')
    parser.add_argument('trainset', metavar='trainset in .csv format', type=str,
                        help='File containing training data')
    parser.add_argument('testset', nargs='?', default='', help='File containing test data')
    parser.add_argument('-b', '--bias', action='store_true',
                        help='Use bias classes')
    parser.add_argument('-p', '--hyperp', action='store_true',
                        help='Use hyperpartisan classes')
    parser.add_argument('-j', '--joint', action='store_true',
                        help='Use joint classes')
    args = parser.parse_args()

    if args.testset:
        print('### found testset, using it... ###')

        df_train = read_data(args.trainset)
        df_train['joint'] = df_train['hyperp'].map(str) + ' ' + df_train['bias']
        df_test = read_data(args.testset)
        df_test['joint'] = df_test['hyperp'].map(str) + ' ' + df_test['bias']

        xtrain = df_train['raw_text']
        ytrain = df_train.ix[:, ['bias', 'hyperp', 'joint']]
        xtest = df_test['raw_text']
        ytest = df_test.ix[:, ['bias', 'hyperp', 'joint']]
        ID = df_train['id']

    else:
        print('### use trainset for training and testing... ###')

        df = read_data(args.trainset)
        df = df.sample(frac=1, random_state=1)
        df['joint'] = df['hyperp'].map(str) + ' ' + df['bias']

        X = df['raw_text']
        Y = df.ix[:, ['bias', 'hyperp', 'joint']]
        ID = df['id']

        xtrain, xtest, ytrain, ytest = train_test_split(X, Y, test_size=0.20)

    weight = None if args.hyperp else 'balanced'

    pipeline = Pipeline([
            ('tfidf', TfidfVectorizer(binary=True, ngram_range=(1,3))),
            ('svc', LinearSVC(class_weight=weight))
        ])

    if args.bias:
        print('Using bias classes..')
        pipeline.fit(xtrain, ytrain.bias)
        yguess_bias = pipeline.predict(xtest)
        print_results(ytest.bias, yguess_bias)

    elif args.hyperp:
        print('Using hyperpartisan classes..')
        pipeline.fit(xtrain, ytrain.hyperp)
        yguess_hyperp = pipeline.predict(xtest)
        print_results(ytest.hyperp, yguess_hyperp)

    elif args.joint:
        print('Using joint classes..')
        pipeline.fit(xtrain, ytrain.joint)
        yguess_joint = pipeline.predict(xtest)
        print_results(ytest.joint, yguess_joint)

        outfile = open(sys.argv[0]+'_output.txt','w')
        print('Writing output to ', outfile.name, '..')
        for index, val in zip(ID, yguess_joint):
            line = str(index) + ' ' + val.lower() + '\n'
            outfile.write(line)
        outfile.close()

    else:
        parser.print_help()
        



